<?php

namespace App\Http\Controllers;

use App\Models\Tareas;
use Illuminate\Http\Request;

class TareasController extends Controller
{
    public function listarTareas()
    {
        $tareas = Tareas::orderBy('prioridad', 'desc')->get();
        return $tareas;
    }

    public function agregarTarea(Request $request)
    {

        if ($request->id == 0) {
            $tarea = new Tareas();
        } else {
            $tarea = Tareas::find($request->id);
        }
        $tarea->titulo = $request->titulo;
        $tarea->descripcion = $request->descripcion;
        $tarea->prioridad = $request->prioridad;

        $tarea->save();
        return response()->json($tarea, 200);
    }

    public function deleteTarea($id)
    {
        Tareas::where('id', $id)->delete();
        return Tareas::all();
    }
}
