<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            $response =
                [
                    'displayName' => $user->name,
                    'userId' => $user->id,
                    'email' => $user->email,
                    'token' => $user->createToken("MyApp")->accessToken,
                    'message' => "Acceso Concedido",
                ];
            return response()->json($response, 200);
        } else {
            $response =
                [
                    'displayName' => '',
                    'userId' => 0,
                    'token' => '',
                    'message' => "Usuario no Encontrado",
                ];
            return response()->json($response, 200);
        }
    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            $response =
                [
                    'message' => "Ok",
                ];
            return response()->json([$response], 200);
        }
    }
}
