<?php

namespace App\Http\Controllers;

use App\Models\contactos;
use Illuminate\Http\Request;

class ContactosController extends Controller
{
    public function listarContactos()
    {
        $contactos=contactos::all();
        return $contactos;
    }

    public function agregarContacto(Request $request){
        
        if($request->id==0){
            $contacto=new contactos();    
        }else{
            $contacto = contactos::find($request->id);
        }        
        $contacto->nombre = $request->nombre;
        $contacto->apellidop = $request->apellidop;
        $contacto->apellidom = $request->apellidom;
        $contacto->telefono = $request->telefono;

        $contacto->save();
        return response()->json($contacto,200);

    }
    public function deleteContacto($id){
        contactos::where('id',$id)->delete();
        return contactos::all();
    }
}
