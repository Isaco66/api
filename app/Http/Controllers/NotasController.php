<?php

namespace App\Http\Controllers;

use App\Models\notas;
use Illuminate\Http\Request;

class NotasController extends Controller
{

    public function listarNotas(){
        return notas::all();
    }

    public function agregarNotas(Request $request){

        if($request->id==0){
            $nota=new notas();
        }else{
            $nota = notas::find($request->id);
        }
        $nota->titulo = $request->titulo;
        $nota->contenido = $request->contenido;

        $nota->save();

        return $nota;

    }

    public function deleteNotas($id){
        notas::where('id',$id)->delete();
        return notas::all();
    }
}
