<?php

use App\Http\Controllers\ContactosController;
use App\Http\Controllers\NotasController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TareasController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login',[LoginController::class,'login']);
Route::get('/logout',[LoginController::class,'logout'])->middleware('auth:api');


Route::get('/listar_contactos',[ContactosController::class,'listarContactos'])->middleware('auth:api');
Route::post('/guardar_contacto',[ContactosController::class,'agregarContacto'])->middleware('auth:api');
Route::delete('/borrar_contacto/{id}',[ContactosController::class,'deleteContacto'])->middleware('auth:api');

Route::get('/listar_notas',[NotasController::class,'listarNotas'])->middleware('auth:api');
Route::post('/guardar_nota',[NotasController::class,'agregarNotas'])->middleware('auth:api');
Route::delete('/borrar_nota/{id}',[NotasController::class,'deleteNotas'])->middleware('auth:api');

Route::get('/listar_tareas',[TareasController::class,'listarTareas'])->middleware('auth:api');
Route::post('/guardar_tarea',[TareasController::class,'agregarTarea'])->middleware('auth:api');
Route::delete('/borrar_tarea/{id}',[TareasController::class,'deleteTarea'])->middleware('auth:api');
